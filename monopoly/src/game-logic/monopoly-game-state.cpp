#include "monopoly-game-state.h"


#include <iostream>
#include <boost/foreach.hpp>

MonopolyModel::MonopolyModel (const std::vector<std::string>& playerNames)
{
	state = std::make_unique<boost::property_tree::ptree>();
	state->put("state.active-player", 0);
	
	BOOST_FOREACH(const std::string & name, playerNames)
		state->add("state.players.player", name);
}

int MonopolyModel::getActivePlayer ()
{
	const auto activePlayer = state->get<int>("state.active-player");
	return activePlayer;
}

int MonopolyModel::getNumPlayers ()
{
	int numPlayers = 0;

	BOOST_FOREACH(boost::property_tree::ptree::value_type & v, state->get_child("state.players"))
	{
		numPlayers++;
	}
	
	return numPlayers;
}

const std::string& MonopolyModel::getPlayerName (int playerNumber)
{
	//auto activePlayer = state->get<int>("state.active-player");
	//std::cout << activePlayer << std::endl;

	//BOOST_FOREACH(boost::property_tree::ptree::value_type & v, state->get_child("state.players"))
	//{
	//	auto name = v.second.data();
	//	std::cout << name << std::endl;
	//}

	return "something";
}
