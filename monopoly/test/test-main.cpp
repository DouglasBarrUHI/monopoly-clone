#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "../src/game-logic/monopoly-game-state.h"

TEST_CASE ("Canary test", "[canary]")
{
    REQUIRE(true);
}

TEST_CASE("A Monopoly model starts with a valid state", "[initialisation]")
{
	std::vector<std::string> playerNames = { "Tom", "Douglas" };
	
	MonopolyModel model (playerNames);
	
	SECTION("The active player value is created and initialised to zero")
	{
		REQUIRE(model.getActivePlayer() == 0);
	}

	SECTION("For the input { Tom, Doug }, two players are created and we can get their names")
	{
		REQUIRE(model.getNumPlayers() == playerNames.size());
		REQUIRE(model.getPlayerName(0) == playerNames[0]);
		REQUIRE(model.getPlayerName(1) == playerNames[1]);
	}
}



//void MonopolyModel::test()
//{
//
//	auto activePlayer = state->get<int>("state.active-player");
//	std::cout << activePlayer << std::endl;
//
//	BOOST_FOREACH(boost::property_tree::ptree::value_type & v, state->get_child("state.players"))
//	{
//		auto name = v.second.data();
//		std::cout << name << std::endl;
//	}
//}